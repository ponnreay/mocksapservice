/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ponnreay.ui;

import com.ponnreay.mocksapservice.ApplicationService;
import java.awt.Color;
import java.awt.Event;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.Document;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

/**
 *
 * @author ponnreay
 */
public class MainFrame extends javax.swing.JFrame {

    private static MainFrame mainFrame;
    private ApplicationService service = null;
    
    private MyPopup menu;
    
    private UndoManager undoManager;
    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        initComponents();
        setLocationRelativeTo(null);
        service = new ApplicationService();
        workingWithUndoRedo();
    }

    private void workingWithUndoRedo(){
        undoManager = new UndoManager();
        
        Document doc = txtAreaMessage.getDocument();
        doc.addUndoableEditListener(new UndoableEditListener() {
            @Override
            public void undoableEditHappened(UndoableEditEvent e) {
                undoManager.addEdit(e.getEdit());
            }
        });
        InputMap inputMap = txtAreaMessage.getInputMap(JComponent.WHEN_FOCUSED);
        ActionMap actionMap = txtAreaMessage.getActionMap();
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "Undo");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() | Event.SHIFT_MASK), "RedoZ");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_Y, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "RedoY");
        
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_C, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "Copy");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_X, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "Cut");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "Paste");
        
        actionMap.put("Undo", new AbstractAction(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (undoManager.canUndo()) {
                        undoManager.undo();
                    }
                } catch (CannotUndoException ex) {
                    printLog(ex.toString());
                }
            }
            
        });
        actionMap.put("RedoZ", new AbstractAction(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (undoManager.canRedo()) {
                        undoManager.redo();
                    }
                } catch (CannotUndoException ex) {
                    printLog(ex.toString());
                }
            }
            
        });
        actionMap.put("RedoY", new AbstractAction(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (undoManager.canRedo()) {
                        undoManager.redo();
                    }
                } catch (CannotUndoException ex) {
                    printLog(ex.toString());
                }
            }
        });
        actionMap.put("Copy", new AbstractAction(){
            @Override
            public void actionPerformed(ActionEvent e) {
                txtAreaMessage.copy();
            }
        });
        actionMap.put("Cut", new AbstractAction(){
            @Override
            public void actionPerformed(ActionEvent e) {
                txtAreaMessage.cut();
            }
        });
        actionMap.put("Paste", new AbstractAction(){
            @Override
            public void actionPerformed(ActionEvent e) {
                txtAreaMessage.paste();
            }
        });
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnStartService = new javax.swing.JButton();
        txtFieldPort = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtAreaMessage = new javax.swing.JTextArea();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtAreaLogs = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        btnStopService = new javax.swing.JButton();
        pwfPassword = new javax.swing.JPasswordField();
        jLabel4 = new javax.swing.JLabel();
        txtFieldUsername = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        panelStatus = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("MockSAPService");

        btnStartService.setText("Start Service");
        btnStartService.setToolTipText("Start service");
        btnStartService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStartServiceActionPerformed(evt);
            }
        });

        txtFieldPort.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtFieldPort.setText("9095");
        txtFieldPort.setToolTipText("Port number ");

        jLabel1.setText("Port");

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        txtAreaMessage.setColumns(20);
        txtAreaMessage.setLineWrap(true);
        txtAreaMessage.setRows(5);
        txtAreaMessage.setText("<SOAP:Envelope \n    xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">\n    <SOAP:Header />\n    <SOAP:Body>\n        <ns1:ISTRUCTURE_Account \n            xmlns:ns1=\"http://soap.sforce.com/schemas/class/skyvvasolutions/IServices\">\n            <Account>\n                <Phone>234365415</Phone>\n                <MESSAGE>failed from server</MESSAGE>\n                <Fax>64542321</Fax>\n            </Account>\n        </ns1:ISTRUCTURE_Account>\n    </SOAP:Body>\n</SOAP:Envelope>");
        txtAreaMessage.setToolTipText("Leave this empty, it will reponse complete message..");
        txtAreaMessage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtAreaMessageMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(txtAreaMessage);

        jLabel2.setText("Message response");

        txtAreaLogs.setEditable(false);
        txtAreaLogs.setColumns(20);
        txtAreaLogs.setRows(5);
        jScrollPane2.setViewportView(txtAreaLogs);

        jLabel3.setText("Logs");

        btnStopService.setText("Stop Service");
        btnStopService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStopServiceActionPerformed(evt);
            }
        });

        pwfPassword.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        pwfPassword.setText("admin");
        pwfPassword.setToolTipText("password");

        jLabel4.setText("username");

        txtFieldUsername.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtFieldUsername.setText("admin");
        txtFieldUsername.setToolTipText("username");

        jLabel5.setText("password");

        jLabel6.setText("Example: http://localhost:9095/test");

        panelStatus.setBackground(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout panelStatusLayout = new javax.swing.GroupLayout(panelStatus);
        panelStatus.setLayout(panelStatusLayout);
        panelStatusLayout.setHorizontalGroup(
            panelStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 32, Short.MAX_VALUE)
        );
        panelStatusLayout.setVerticalGroup(
            panelStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 29, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jScrollPane2)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGap(34, 34, 34)
                                        .addComponent(jLabel1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                                    .addGroup(layout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(jLabel4)
                                        .addGap(4, 4, 4)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtFieldPort, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtFieldUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(pwfPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnStartService)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnStopService)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(panelStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 13, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnStartService)
                        .addComponent(txtFieldPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)
                        .addComponent(btnStopService))
                    .addComponent(panelStatus, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtFieldUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pwfPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addGap(14, 14, 14)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnStartServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStartServiceActionPerformed
        
        panelStatus.setBackground(Color.green);
        try{
            int port = Integer.parseInt(txtFieldPort.getText());
            String username = txtFieldUsername.getText();
            String password = new String(pwfPassword.getPassword());
            service.setUsername(username);
            service.setPassword(password);
            service.setPort(port);
            service.restart();
            printLog("Service started.....");
            btnStartService.setEnabled(false);
            btnStopService.setEnabled(true);
        }catch(NumberFormatException e){
            printLog(e.toString());
        }
        
    }//GEN-LAST:event_btnStartServiceActionPerformed

    private void btnStopServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStopServiceActionPerformed

        service.stop();
        panelStatus.setBackground(Color.red);
        printLog("bye bye.....");
        btnStartService.setEnabled(true);
        btnStopService.setEnabled(false);
    }//GEN-LAST:event_btnStopServiceActionPerformed

    private void txtAreaMessageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtAreaMessageMouseClicked
        if(evt.getButton() == MouseEvent.BUTTON3){
            
            doPopup(evt);
            
        }
    }//GEN-LAST:event_txtAreaMessageMouseClicked

    public static MainFrame getInstance(){
        return mainFrame;
    }
    public void printLog(String msg) {
        if(txtAreaLogs.getRows()>100){
            txtAreaLogs.setText(msg+"\n");
        }else{
            txtAreaLogs.append(msg+"\n");
        }
    }
    
    public String getMessage(){
        
        return txtAreaMessage.getText();
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                mainFrame = new MainFrame();
                mainFrame.setVisible(true);
            }
        });
    }
    private void doPopup(MouseEvent evt) {
        if(menu==null) menu = new MyPopup();
        menu.show(txtAreaMessage, evt.getX(), evt.getY());
    }
   
    private class MyPopup extends JPopupMenu {
        JMenuItem copy;
        JMenuItem cut;
        JMenuItem paste;
        JMenuItem undo;
        JMenuItem redo;
        public MyPopup(){
            copy = new JMenuItem(new DefaultEditorKit.CopyAction());
            cut = new JMenuItem(new DefaultEditorKit.CutAction());
            paste = new JMenuItem(new DefaultEditorKit.PasteAction());
            copy.setText("Copy");
            cut.setText("Cut");
            paste.setText("Paste");
            
            undo = new JMenuItem("Undo");
            redo = new JMenuItem("Redo");
            initAction();
            add(copy);
            add(cut);
            add(paste);
            add(undo);
            add(redo);
            
        }
        private void initAction(){
   
            undo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        if (undoManager.canUndo()) {
                            undoManager.undo();
                        }
                    } catch (CannotUndoException ex) {
                        printLog(ex.toString());
                    }
                }
            });
            redo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        if (undoManager.canRedo()) {
                            undoManager.redo();
                        }
                    } catch (CannotUndoException ex) {
                        printLog(ex.toString());
                    }
                }
            });
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnStartService;
    private javax.swing.JButton btnStopService;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel panelStatus;
    private javax.swing.JPasswordField pwfPassword;
    private javax.swing.JTextArea txtAreaLogs;
    private javax.swing.JTextArea txtAreaMessage;
    private javax.swing.JTextField txtFieldPort;
    private javax.swing.JTextField txtFieldUsername;
    // End of variables declaration//GEN-END:variables

}
