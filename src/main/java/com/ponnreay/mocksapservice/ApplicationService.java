package com.ponnreay.mocksapservice;

import com.ponnreay.ui.MainFrame;
import com.qmetric.spark.authentication.AuthenticationDetails;
import com.qmetric.spark.authentication.BasicAuthenticationFilter;
import spark.Spark;
import static spark.Spark.*;

public class ApplicationService {

    private int port;
    private boolean started;

    private String username;
    private String password;
         
    
    
    public ApplicationService(){
        
    }
    
    public ApplicationService(int port) {
        
           
    }

    public void restart(){
        port(this.port);
        started = true;
        before(new BasicAuthenticationFilter("/path/*", new AuthenticationDetails(username, password)));
        
        
        get("/test", (req, res)->{
           MainFrame.getInstance().printLog(req.url()+ " - "+ req.userAgent());
           res.type("application/xml");
           return MainFrame.getInstance().getMessage(); 
        });
        post("/test", (req, res)->{
           MainFrame.getInstance().printLog(req.url()+ " - "+ req.userAgent());
           res.type("application/xml");
           return MainFrame.getInstance().getMessage(); 
        });
        
    }
    
    public boolean isStart(){
        
        return started;
    }
    public void stop(){
        started = false;
        Spark.stop();
    }
    public void setPort(int port) {
        this.port = port;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
